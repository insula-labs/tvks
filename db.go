package tvks

import (
	"database/sql"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"log/slog"
	"time"
)

const statement_create_table_groups = `create table groups (
  id integer not null primary key,
  name text,
  UNIQUE(name)
)`

func statement_create_table_submissions(name string) string {
	return fmt.Sprintf(
		`create table %s (
  key datetime not null primary key,
  value blob,
  meta blob
)`, name)
}

func build_statement_atom_submissions_table(name string) string {
	return fmt.Sprintf("%s_submissions", name)
}

const statement_get_table_exist = `select name from sqlite_master where type = 'table' and name = ?`

const statement_retrieve_groups = `select id, name from groups`

const statement_retrieve_group_id_by_name = `select id from groups where name = ?`

const statement_new_group = `insert into groups (id, name) values (NULL, ?)`

func build_statement_atom_submissions_insert(name string) string {
	return fmt.Sprintf("insert into %s_submissions (key, value, meta) values (?, ?, ?)", name)
}

func build_statement_atom_submissions_retrieve_entry_by_key(table *string) string {
	return fmt.Sprintf("select key, value, meta from %s where key = ?", *table)
}

func build_statement_atom_submissions_retrieve_entries_by_range(table *string) string {
	return fmt.Sprintf("select key, value, meta from %s where key >= ? and key <= ?", *table)
}

func db_open(path string, maxCons int) (*sql.DB, error) {

	slog.Debug("db_open")

	const options = "?_journal_mode=WAL"
	var err error

	db, err := sql.Open("sqlite3", fmt.Sprintf("%s%s", path, options))
	if err != nil {

		return nil, err
	}
	db.SetMaxOpenConns(maxCons)
	return db, nil
}

func db_does_table_exist(db *sql.DB, table string) (bool, error) {

	slog.Debug("db_does_table_exist")

	stmt, err := db.Prepare(statement_get_table_exist)

	if err != nil {
		return false, err
	}
	defer stmt.Close()

	var name string
	err = stmt.QueryRow(table).Scan(&name)
	if err == nil {
		return true, nil
	}
	return false, nil
}

func db_ensure_table_exists(db *sql.DB, table string, creation_stmt string) error {

	slog.Debug("db_ensure_table_exists")

	exists, err := db_does_table_exist(db, table)
	if err != nil {
		return err
	}

	if exists {
		return nil
	}

	_, err = db.Exec(creation_stmt)
	if err != nil {

		return err
	}

	return nil
}

func db_retrieve_group_info_by_name(db *sql.DB, group string) (*groupInfo, error) {

	slog.Debug("db_retrieve_group_info_by_name")

	stmt, err := db.Prepare(statement_retrieve_group_id_by_name)

	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	info := groupInfo{
		0,
		group,
		build_statement_atom_submissions_insert(group),
	}

	err = stmt.QueryRow(group).Scan(&info.id)
	if err == nil {
		return &info, nil
	}

	return nil, nil
}

func db_ensure_group_exists(db *sql.DB, groupName string) error {

	slog.Debug("db_ensure_group_exists")

	entry, err := db_retrieve_group_info_by_name(db, groupName)
	if err != nil {
		return err
	}

	if entry != nil {
		return nil
	}

	db_store_new_group(db, groupName)
	return nil
}

func db_store_new_group(db *sql.DB, name string) error {

	slog.Debug("db_store_new_group")

	tx, err := db.Begin()
	if err != nil {
		return err
	}

	stmt, err := tx.Prepare(statement_new_group)
	if err != nil {

		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(name)
	err = tx.Commit()
	if err != nil {
		return err
	}

	return nil
}

func db_retrieve_groups(db *sql.DB, estimatedGroupSize int) (*[]groupInfo, error) {

	slog.Debug("db_retrieve_groups")

	rows, err := db.Query(statement_retrieve_groups)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	result := make([]groupInfo, 0, estimatedGroupSize)

	for rows.Next() {
		entry := groupInfo{0, "", ""}
		err = rows.Scan(&entry.id, &entry.name)
		if err != nil {

			return nil, err
		}

		entry.insertionStatement = build_statement_atom_submissions_insert(entry.name)

		result = append(result, entry)
	}

	err = rows.Err()
	if err != nil {
		return nil, err
	}

	return &result, nil
}

func db_store_entry(db *sql.DB, statement *string, kv *KeyValue) error {

	slog.Debug("db_store_entry")

	tx, err := db.Begin()
	if err != nil {

		return err
	}

	stmt, err := tx.Prepare(*statement)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(kv.Key, kv.Value, kv.Meta)
	err = tx.Commit()
	if err != nil {
		return err
	}

	return nil
}

func db_retrieve_key(db *sql.DB, table *string, key *time.Time) (*KeyValue, error) {

	slog.Debug("db_retrieve_key")

	stmt, err := db.Prepare(build_statement_atom_submissions_retrieve_entry_by_key(table))

	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	var kv KeyValue

	err = stmt.QueryRow(key).Scan(&kv.Key, &kv.Value, &kv.Meta)
	if err != nil {
		return nil, err
	}

	return &kv, nil
}

func db_retrieve_range(db *sql.DB, table *string, start *time.Time, end *time.Time) (*[]*KeyValue, error) {

	slog.Debug("db_retrieve_range")

	rows, err := db.Query(
		build_statement_atom_submissions_retrieve_entries_by_range(table),
		*start,
		*end)

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	result := make([]*KeyValue, 0, 5)

	for rows.Next() {
		var kv KeyValue
		err = rows.Scan(&kv.Key, &kv.Value, &kv.Meta)
		if err != nil {

			return nil, err
		}

		result = append(result, &kv)
	}

	err = rows.Err()
	if err != nil {
		return nil, err
	}

	return &result, nil
}
