package tvks

import (
	"time"
)

// Specify a time range for a query
type QueryRange struct {
	Start *time.Time
	End   *time.Time
}

// Query into the Db
type Query struct {
	RefId  uint64      // Reference ID that query builder can use to match query with result
	Groups *[]string   // List of groups to run the query against
	Range  *QueryRange // Inclusive time range for query
}

// Resulting data from query
type QueryData struct {
	ResultCode int
	Values     []*KeyValue
}

type QueryResult struct {
	RefId  uint64               // User-defined id to match result to query
	Result map[string]QueryData // Results per-group sekectuib
}

func QueryBetween(
	ref uint64,
	start time.Time,
	end time.Time,
	groups *[]string) *Query {

	var span QueryRange
	span.Start = &start
	span.End = &end
	return &Query{
		ref,
		groups,
		&span,
	}
}

func QueryAt(
	ref uint64,
	start time.Time,
	groups *[]string) *Query {

	var span QueryRange
	span.Start = &start
	span.End = nil
	return &Query{
		ref,
		groups,
		&span,
	}
}

func QueryBefore(
	ref uint64,
	start time.Time,
	groups *[]string) *Query {

	return QueryBetween(
		ref,
		start.AddDate(-500, 0, 0),
		start,
		groups)
}

func QueryAfter(
	ref uint64,
	start time.Time,
	groups *[]string) *Query {

	return QueryBetween(
		ref,
		start,
		start.AddDate(500, 0, 0),
		groups)
}
