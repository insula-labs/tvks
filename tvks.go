// Package tvks provides a simple-to-use immutable time-series
// blob database
package tvks

import (
	"database/sql"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"log/slog"
	"time"
)

// Result codes that are returned in
// an Error as-well-as QueryData
const (
	TvksDuplicateKey = iota
	TvksItemExists
	TvksItemNotFound
	TvksInternalError
	TvksOk
)

// Error that may occur when interacting with tvks
type Error struct {
	Id    int
	Value error
}

// Thg base key/ value structure that is used to
// represent records
type KeyValue struct {
	Key   time.Time // Time key
	Value []byte    // Nebulous data
	Meta  []byte    // Nebulous meta data
}

type TvksRead interface {
	ExecuteQuery(query *Query) (*QueryResult, *Error)
}

type TvksWrite interface {
	Store(group *string, data *KeyValue) *Error
}

type TvksReadWrite interface {
	TvksRead
	TvksWrite
}

// Actual database object that holds records
type Db struct {
	readOnly  *sql.DB
	readWrite *sql.DB

	groupIdCache   map[string]int
	groupCacheData map[int]cacheData
}

// Open a database and give a list of groups that are expected
// to exist there. If the group does not exist yet, they will be
// made
func Open(path string, groups []string) (*Db, error) {

	slog.Debug("TVKS Open")

	tdb := Db{
		nil,
		nil,
		make(map[string]int),
		make(map[int]cacheData),
	}

	var err error

	tdb.readWrite, err = db_open(path, 1)
	if err != nil {
		return nil, err
	}

	tdb.readOnly, err = db_open(path, 5000)
	if err != nil {
		return nil, err
	}

	err = db_ensure_table_exists(
		tdb.readWrite,
		"groups",
		statement_create_table_groups)

	if err != nil {
		return nil, err
	}

	for _, group := range groups {

		submissionName := build_statement_atom_submissions_table(group)

		slog.Debug("ensuring tables exist....")
		db_ensure_table_exists(
			tdb.readWrite,
			submissionName,
			statement_create_table_submissions(
				submissionName))

		slog.Debug("ensuring groups exist....")
		db_ensure_group_exists(
			tdb.readWrite,
			group)
	}

	slog.Debug("populating cache....")

	// populate group data cache.
	// - We store group data in database for reloading,
	//   but groups are only ever made (for now) at process
	//   start so we will save them to DB but use the cache
	//   to transform queries before writing

	var allGroups *[]groupInfo
	allGroups, err = db_retrieve_groups(tdb.readOnly, len(groups))

	if err != nil {
		return nil, err
	}

	for _, info := range *allGroups {
		tdb.updateCache(&info)
	}

	return &tdb, nil
}

// Close the database
func (tdb *Db) Close() {

	slog.Debug("TVKS Close")

	if tdb.readOnly != nil {
		tdb.readOnly.Close()
		tdb.readOnly = nil
	}
	if tdb.readWrite != nil {
		tdb.readWrite.Close()
		tdb.readWrite = nil
	}
}

// Store a record in the database unser a its correlated group
func (tdb *Db) Store(group *string, value *KeyValue) *Error {

	slog.Debug("TVKS Store")

	id, dbErr := tdb.getGroupId(group)

	if dbErr != nil {
		return dbErr
	}

	gcd, ok := tdb.groupCacheData[id]
	if !ok {
		return make_db_err(
			TvksItemNotFound,
			fmt.Errorf(
				"Unable to retrieve cache entry for group: %s",
				*group),
		)
	}

	err := db_store_entry(tdb.readWrite, &gcd.insertionStatement, value)

	if err != nil {
		slog.Debug("Failed to insert the data:", err)
		return make_db_err(TvksInternalError, err)
	}

	return nil
}

// Submit a retrieval query to the database. Queries do no modify the
// data
func (tdb *Db) ExecuteQuery(query *Query) (*QueryResult, *Error) {

	result := QueryResult{
		query.RefId,
		make(map[string]QueryData),
	}

	for _, group := range *query.Groups {

		queryData := QueryData{
			TvksOk,
			make([]*KeyValue, 0, 20), // TODO: make const for this
		}

		// Single point in time
		if query.Range.End == nil {

			val, err := tdb.getSingleEntry(&group, query.Range.Start)
			if err != nil {
				queryData.ResultCode = err.Id
				slog.Debug("Query Execution Error: ", err.Value)
				queryData.Values = nil
			} else {
				queryData.Values = make([]*KeyValue, 0, 1)
				queryData.Values = append(queryData.Values, val)
			}
			result.Result[group] = queryData
			continue
		}

		// Range point in time

		values, err := tdb.getRangeEntry(&group, query.Range.Start, query.Range.End)

		if err != nil {
			return nil, err
		}

		queryData.Values = *values
		result.Result[group] = queryData
	}

	return &result, nil
}

func (e *Error) String() string {
	if e.Value != nil {
		return fmt.Sprintf("%s: %v", resultCodeToString(e.Id), e.Value)
	}
	return resultCodeToString(e.Id)
}

func (tdb *Db) getSingleEntry(group *string, key *time.Time) (*KeyValue, *Error) {

	id, dbErr := tdb.getGroupId(group)

	if dbErr != nil {
		return nil, dbErr
	}

	gcd, ok := tdb.groupCacheData[id]
	if !ok {
		return nil, make_db_err(
			TvksInternalError,
			fmt.Errorf("Unable to retrieve cache entry for group: %s", *group))
	}

	kv, err := db_retrieve_key(tdb.readOnly, &gcd.tableName, key)

	if err != nil {
		return nil, make_db_err(TvksItemNotFound, err)
	}

	return kv, nil
}

func (tdb *Db) getRangeEntry(group *string, start *time.Time, end *time.Time) (*[]*KeyValue, *Error) {

	id, dbErr := tdb.getGroupId(group)

	if dbErr != nil {
		return nil, dbErr
	}

	gcd, ok := tdb.groupCacheData[id]
	if !ok {
		return nil, make_db_err(
			TvksInternalError,
			fmt.Errorf("Unable to retrieve cache entry for group: %s", *group))
	}

	results, err := db_retrieve_range(tdb.readOnly, &gcd.tableName, start, end)

	if err != nil {
		return nil, make_db_err(TvksItemNotFound, err)
	}

	return results, nil
}

func (tdb *Db) updateCache(entry *groupInfo) {

	slog.Debug("TVKS updateCache")

	tdb.groupIdCache[entry.name] = entry.id
	tdb.groupCacheData[entry.id] = cacheData{
		entry.insertionStatement,
		build_statement_atom_submissions_table(entry.name),
	}
}

func (tdb *Db) getGroupId(name *string) (int, *Error) {
	slog.Debug("TVKS getGroupId")

	id, ok := tdb.groupIdCache[*name]
	if ok {
		return id, nil
	}

	slog.Debug("cache miss")

	entry, err := db_retrieve_group_info_by_name(tdb.readOnly, *name)

	if err != nil {
		return 0, make_db_err(TvksItemNotFound, fmt.Errorf("Group not found: %s", *name))
	}

	tdb.updateCache(entry)

	return entry.id, nil
}

type groupInfo struct {
	id                 int
	name               string
	insertionStatement string
}

type cacheData struct {
	insertionStatement string
	tableName          string

	// Could store last handful of queried data
	// and attempt to bypass the reads from database
	// as time submission data is immutable
}

func resultCodeToString(code int) string {
	switch code {
	case TvksDuplicateKey:
		return "TvksDuplicateKey"
	case TvksItemExists:
		return "TvksItemExists"
	case TvksItemNotFound:
		return "TvksItemNotFound"
	case TvksInternalError:
		return "TvksInternalError"
	case TvksOk:
		return "TvksOk"
	}
	return "TvksInternalError"
}

func make_db_err(id int, err error) *Error {
	var e Error
	e.Id = id
	e.Value = err
	return &e
}
