package tvks

import (
	"fmt"
	"log/slog"
	"math/rand"
	"os"
	"path/filepath"
	"sync"
	"testing"
	"time"
)

const testNumSensors = 4096
const testNumReaders = 32
const testMaxInterval = 10
const testRunTimeSec = 20

type syncData struct {
	stopChan <-chan int
	wg       *sync.WaitGroup
}

type sensorData struct {
	syncData

	id          int
	intervalSec time.Duration
	message     string
	meta        string
	group       string
}

type sensorReader struct {
	syncData

	intervalSec time.Duration
	groups      *[]string
}

func generateSensorReaders(n int, sensors *[]sensorData, groups *[]string, sd *syncData) []sensorReader {

	result := make([]sensorReader, 0, n)

	for i := 0; i < n; i++ {
		currentReader := sensorReader{
			*sd,
			time.Duration(rand.Intn(testMaxInterval)),
			groups,
		}
		result = append(result, currentReader)
	}
	return result
}

func generateSensorGroups(n int, sd *syncData) ([]sensorData, []string) {

	groups := make([]string, 0, n)
	sensors := make([]sensorData, 0, n)

	for i := 0; i < n; i++ {
		group := fmt.Sprintf("sensor_%d", i)
		currentSensor := sensorData{
			*sd,
			i,
			time.Duration(rand.Intn(testMaxInterval) + 1),
			fmt.Sprintf("sensor:%d", i),
			fmt.Sprintf("meta:%d", i),
			group,
		}
		groups = append(groups, group)
		sensors = append(sensors, currentSensor)
	}
	return sensors, groups
}

func sensorRunner(
	writer TvksWrite,
	sensor sensorData) {

	sensor.wg.Add(1)
	defer sensor.wg.Done()

	for {

		newReport := KeyValue{
			time.Now(),
			[]byte(sensor.message),
			[]byte(sensor.meta),
		}

		err := writer.Store(&sensor.group, &newReport)
		if err != nil {
			slog.Info("failed to write to db", "err", err.String())
			return
		}

		select {
		case msg := <-sensor.stopChan:
			slog.Info("sensor stop", "sensor", sensor.id, "msg", msg)
			return
		default:
			break
		}

		time.Sleep(sensor.intervalSec * time.Second)
	}
}

func readerRunner(
	reader TvksRead,
	sensor sensorReader) {

	sensor.wg.Add(1)
	defer sensor.wg.Done()

	for {

		var query *Query
		id := uint64(rand.Intn(1024))
		qselect := rand.Intn(2)
		switch qselect {
		case 0:
			query = QueryAfter(
				id,
				time.Now(),
				sensor.groups)
			break
		case 1:
			query = QueryBefore(
				id,
				time.Now(),
				sensor.groups)
			break
		}

		out := fmt.Sprintf("Before %s", query.Range.Start)
		if qselect == 0 {
			out = fmt.Sprintf("After %s", query.Range.Start)
		}

		qStart := time.Now()
		result, dbErr := reader.ExecuteQuery(query)
		qEnd := time.Now()

		if dbErr != nil {
			slog.Error(dbErr.String())
			return
		}

		if result == nil {
			slog.Error("Received null query response")
			return
		}

		if result.RefId != id {
			slog.Error("Received incorrect id")
			return
		}

		sum := 0
		for _, x := range result.Result {
			sum += len(x.Values)
		}

		slog.Info(
			"reader complete",
			"out", out,
			"id", id,
			"results", sum,
			"time", qEnd.Sub(qStart))

		select {
		case msg := <-sensor.stopChan:
			slog.Info("sensor reader stop", "msg", msg)
			return
		default:
			break
		}
		time.Sleep(sensor.intervalSec * time.Second)
	}
}

func TestTvksDbStress(t *testing.T) {
	lvl := new(slog.LevelVar)
	lvl.Set(slog.LevelDebug)

	logger := slog.New(slog.NewTextHandler(
		os.Stderr,
		&slog.HandlerOptions{Level: lvl}))

	slog.SetDefault(logger)

	stopChan := make(chan int)
	sd := syncData{
		stopChan,
		&sync.WaitGroup{},
	}

	sensors, groups := generateSensorGroups(testNumSensors, &sd)
	readers := generateSensorReaders(testNumReaders, &sensors, &groups, &sd)

	db, err := Open(
		filepath.Join(t.TempDir(), dbFileNameQT),
		groups)

	defer db.Close()

	if err != nil {
		t.Fatalf(`Error: %v`, err)
	}

	slog.Debug("TestTvksDbStress", "sensors", len(sensors))

	for _, sensor := range sensors {
		go sensorRunner(
			db,
			sensor)
	}

	for _, reader := range readers {
		go readerRunner(
			db,
			reader)
	}

	time.Sleep(time.Duration(testRunTimeSec) * time.Second)

	slog.Debug("stopping")

	for _, _ = range sensors {
		stopChan <- 0
	}

	for _, _ = range readers {
		stopChan <- 0
	}

	slog.Debug("waiting..")

	sd.wg.Wait()
}
