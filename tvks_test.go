package tvks

import (
	"fmt"
	"log/slog"
	"os"
	"path/filepath"
	"testing"
	"time"
)

const numTestDbEntries = 255
const timeLayout = "2006-01-02T15:04:05.000Z"
const timeSeed = "2024-01-31T15:07:59.000Z" // Happy Birthday, D
const dbFileNameQT = "query_test.db"

type queryTestCase struct {
	query    *Query
	expected *QueryResult
}

type groupData struct {
	group string
	data  []KeyValue
}

func TestTvksDb(t *testing.T) {
	lvl := new(slog.LevelVar)
	lvl.Set(slog.LevelDebug)

	logger := slog.New(slog.NewTextHandler(
		os.Stderr,
		&slog.HandlerOptions{Level: lvl}))

	slog.SetDefault(logger)

	realizedTime, err := time.Parse(timeLayout, timeSeed)
	if err != nil {
		t.FailNow()
	}

	groups := []string{
		"g0",
		"g1",
		"g2",
		"g3",
		"g4",
	}

	tdb, err := Open(
		filepath.Join(t.TempDir(), dbFileNameQT),
		groups)

	defer tdb.Close()

	if err != nil {
		t.Fatalf(`Error: %v`, err)
	}

	testingData := generateTestData(realizedTime, groups)

	populateDb(t, tdb, testingData)

	doAllQueryAt(t, tdb, &groups, testingData)

	doQuery(t, tdb, QueryAfter(
		0x45,
		testingData[0].data[0].Key,
		&groups),
		&groups,
		testingData,
		0,
		numTestDbEntries,
		numTestDbEntries)

	doQuery(t, tdb, QueryBefore(
		0x1A4,
		testingData[0].data[numTestDbEntries-1].Key,
		&groups),
		&groups,
		testingData,
		0,
		numTestDbEntries,
		numTestDbEntries)

	block := numTestDbEntries / 4
	doQuery(t, tdb, QueryBetween(
		0x10F2C,
		testingData[0].data[block].Key,
		testingData[0].data[block*2].Key,
		&groups),
		&groups,
		testingData,
		block,
		block*2,
		block+1) // Add one because selection is inclusive
}

func doQuery(
	t *testing.T,
	db *Db,
	query *Query,
	groups *[]string,
	data []groupData,
	expectedStartRange int,
	expectedEndRange int,
	expectedResults int) {

	result, dbErr := db.ExecuteQuery(query)

	if dbErr != nil {
		t.Fatalf("%s", dbErr.String())
	}

	if result == nil {
		t.Fatalf("Nil query response [%d]", query.RefId)
	}

	if result.RefId != uint64(query.RefId) {
		t.Fatalf("Reference Id Mismatch. Expected [%d] Got [%d]",
			query.RefId,
			result.RefId)
	}

	for groupNum, group := range *groups {

		value, ok := result.Result[group]

		if !ok {
			t.Fatalf("Failed to obtain entry result for %s", group)
		}

		if value.ResultCode != TvksOk {
			t.Fatalf("Unexpected result code. Expected [%s] Got [%s]",
				resultCodeToString(TvksOk),
				resultCodeToString(value.ResultCode))
		}

		if len(value.Values) != expectedResults {
			t.Fatalf("Expected %d results, but got %d for group %s",
				expectedResults,
				len(value.Values),
				group)
		}

		resultNum := 0
		for num := expectedStartRange; num < expectedEndRange; num++ {

			storedData := value.Values[resultNum]
			entry := data[groupNum].data[num]
			resultNum++

			if storedData.Key.Format(time.RFC3339) != entry.Key.Format(time.RFC3339) {
				t.Fatalf(
					`Expected key %s, but got key %s [%d]`,
					entry.Key,
					storedData.Key,
					num)
			}

			if string(storedData.Value) != string(entry.Value) {
				t.Fatalf("Expected data:\n\t [%s]\nGot data:\n\t[%s]\n\n.",
					string(entry.Value),
					string(storedData.Value))
			}
			if string(storedData.Meta) != string(entry.Meta) {
				t.Fatalf("Expected meta:\n\t [%s]\nGot meta:\n\t[%s]\n\n.",
					string(entry.Meta),
					string(storedData.Meta))
			}
		}
	}
}

func doAllQueryAt(t *testing.T, db *Db, groups *[]string, data []groupData) {

	// Individual group search
	for _, group := range data {
		for dataNum, entry := range group.data {
			query := QueryAt(uint64(dataNum),
				entry.Key,
				groups)
			doQuery(t, db, query, groups, data, 0, 0, 1)
		}
	}
}

func populateDb(t *testing.T, db *Db, data []groupData) {
	for _, datum := range data {
		for _, val := range datum.data {
			err := db.Store(&datum.group, &val)
			if err != nil {
				slog.Debug(err.String())
				t.FailNow()
			}
		}
	}
}

func generateTestData(initialTime time.Time, testGroupNames []string) []groupData {

	result := make([]groupData, 0, len(testGroupNames))
	for _, g := range testGroupNames {
		data := groupData{g, make([]KeyValue, 0, numTestDbEntries)}
		for i := 0; i < numTestDbEntries; i++ {
			kv := KeyValue{
				initialTime.Add(time.Duration(time.Duration(i) * time.Minute)),
				[]byte(fmt.Sprintf("Test data: %d", i)),
				[]byte(fmt.Sprintf("meta: %d", i)),
			}
			data.data = append(data.data, kv)
		}
		result = append(
			result,
			data)
	}
	return result
}
